package logan.ch5;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

public class LogAnalyzerTests {

    @Test
    public void analyze_tooShortFileNAme_callLogger() {
        //FakeLogger logger = new FakeLogger();
        ILogger logger = Mockito.mock(ILogger.class);
        LogAnalyzer analyzer = new LogAnalyzer(logger);
        analyzer.minNameLength = 6;
        boolean result = analyzer.analyze("a.txt");

        Assert.assertFalse(result);
    }

    @Test
    public void returns_byDefault_worksForHardCodedArgument() {
        IFileNameRules fakeRules = Mockito.mock(IFileNameRules.class);
        Mockito.when(fakeRules.isValidLogFileName(Mockito.anyString())).thenReturn(true);
        boolean result = fakeRules.isValidLogFileName("anything.txt");
        Assert.assertTrue(result);
    }


    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Test
    public void returns_argAny_throws() {
        IFileNameRules fakeRules = Mockito.mock(IFileNameRules.class);
        Mockito.doThrow(new RuntimeException()).when(fakeRules).isValidLogFileName(Mockito.anyString());
        thrown.expect(RuntimeException.class);

        fakeRules.isValidLogFileName("abc.txt");
    }

//    class FakeLogger implements ILogger {
//
//        public String lastError;
//        @Override
//        public void logError(String message) {
//            this.lastError = message;
//        }
//    }
}
