package logan.ch5;

import org.junit.Test;
import org.mockito.Mockito;

public class EventListenerTests {

    @Test
    public void testOpenEvent_returnString() {
        IView event = Mockito.mock(IView.class);
        Mockito.when(event.render("hello")).thenReturn(null);

        Presenter listener = new Presenter();
        listener.onLoad(event);

        Mockito.verify(event).render("hello");
    }
}
