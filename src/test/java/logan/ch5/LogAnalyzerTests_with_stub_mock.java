package logan.ch5;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

public class LogAnalyzerTests_with_stub_mock {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void analyze_loggerThrows_callsWebService() {
        IWebService mockWebService = Mockito.mock(IWebService.class);
        ILogger stubLogger = Mockito.mock(ILogger.class);
        Mockito.doThrow(new RuntimeException("fake exception")).when(stubLogger).logError(Mockito.anyString());

        LogAnalyzer2 analyzer = new LogAnalyzer2(stubLogger, mockWebService);
        analyzer.minNameLength = 10;
        analyzer.analyze("short.txt");

        Mockito.verify(mockWebService).write("error from Logger:" + new RuntimeException("fake exception"));
    }
}
