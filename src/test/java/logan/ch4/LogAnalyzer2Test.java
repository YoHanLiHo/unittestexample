package logan.ch4;

import org.junit.Assert;
import org.junit.Test;

public class LogAnalyzer2Test {

    @Test
    public void analyze_webServiceThrows_sendsEmail() {
        FakeWebService stubService = new FakeWebService();
        stubService.toThrow = new Exception("fake exception");

        FakeEmailService mockEmail = new FakeEmailService();
        LogAnalyzer2 log = new LogAnalyzer2(stubService, mockEmail);

        String tooShortFileName = "abc.ext";
        log.analyze(tooShortFileName);

        Assert.assertEquals("someone@somewhere.com", mockEmail.to);
        Assert.assertEquals("fake exception", mockEmail.body);
        Assert.assertEquals("can't log", mockEmail.subject);
    }


    /**
     * inner class
     */
    public class FakeWebService implements IWebService {

        public Exception toThrow;

        @Override
        public void logError(String message) throws Exception {
            if (toThrow != null) {
                throw toThrow;
            }
        }
    }

    public class FakeEmailService implements IEmailService {

        public String to;
        public String subject;
        public String body;

        @Override
        public void sendMail (String to, String subject, String body) {
            this.to = to;
            this.subject = subject;
            this.body = body;
        }

    }
}
