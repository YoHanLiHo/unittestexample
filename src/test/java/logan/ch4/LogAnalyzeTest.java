package logan.ch4;

import org.junit.Assert;
import org.junit.Test;

public class LogAnalyzeTest {

    @Test
    public void analyze_tooShortFileName_callWebService() {

        /**
         * This fake object is mock object during runtime.
         * **/
        FakeWebService mockService = new FakeWebService();

        LogAnalyze log = new LogAnalyze(mockService);

        String tooShortFileNAme = "abc.ext";
        try {
            log.analyze(tooShortFileNAme);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.assertEquals("Filename too short:abc.ext", mockService.lastError);
    }
}