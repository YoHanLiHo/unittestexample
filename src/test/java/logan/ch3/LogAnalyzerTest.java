package logan.ch3;

import org.junit.Assert;
import org.junit.Test;

public class LogAnalyzerTest {

    @Test
    public void isValidFileName_nameSupportedExtension_returnTrue() {
        FakeExtensionManager myFakeManager = new FakeExtensionManager();
        myFakeManager.willBeValid = true;
        LogAnalyzer log = new LogAnalyzer(myFakeManager);

        boolean result = log.isValidLogFileName("short.ext");

        Assert.assertTrue(result);
    }

    @Test
    public void isValidFileName_supportedExtension_returnsTrue() {
        FakeExtensionManager myFakeManager = new FakeExtensionManager();
        myFakeManager.willBeValid = true;
        LogAnalyzer log = new LogAnalyzer();

        log.setManager(myFakeManager);

        boolean result = log.isValidLogFileName("short.ext");
        Assert.assertTrue(result);
    }

    @Test
    public void overrideTest() {
        FakeExtensionManager stub = new FakeExtensionManager();
        stub.willBeValid = true;

        /**
         * TestableLogAnalyzer 是繼承專案中的被測試類別，如此測試程式就能在不影響被測試類別的情況下進行測試
         */
        TestableLogAnalyzer logan = new TestableLogAnalyzer(stub);
        boolean result = logan.isValidLogFileName("file.ext");

        Assert.assertTrue(result);
    }

    /**
     * inner class
     */

    /**
     * 因為目前只有這一個 test 在使用，所以我們用 inner class 也便於查看測試用的元件，增加可讀性
     * */
    private class FakeExtensionManager implements IExtensionManager {

        public boolean willBeValid = false;

        @Override
        public boolean isValid(String fileName) {
            return true;
        }

    }

    class TestableLogAnalyzer extends LogAnalyzerUsingFactoryMethod {

        public IExtensionManager manager;

        public TestableLogAnalyzer(IExtensionManager mgr) {
            this.manager = mgr;
        }

        @Override
        protected IExtensionManager getManager() {
            return manager;
        }
    }
}