package logan.ch3.code_3_8;

import org.junit.Assert;
import org.junit.Test;

public class LogAnalyzerWithoutFactoryMethodTest {

    @Test
    public void overrideTestWithoutStub() {
        TestableLogAnalyzer logan = new TestableLogAnalyzer();
        logan.isSupported = true;
        boolean result = logan.isValidLogFileName("file.ext");

        Assert.assertTrue(result);
    }

    class TestableLogAnalyzer extends LogAnalyzerWithoutFactoryMethod {

        public boolean isSupported;

        @Override
        protected boolean isValid(String fileName) {
            return isSupported;
        }
    }
}
