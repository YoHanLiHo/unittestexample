package logan.ch3;

import org.junit.Assert;
import org.junit.Test;

public class LogAnalyzer_3_6Test {

    @Test
    public void isValidFileName_supportedExtension_returnsTrue() {
        FakeExtensionManager myFakeManager = new FakeExtensionManager();
        myFakeManager.willBeValid = true;

        /**
         * 執行期才注入要用的 manager 物件實體
         *  */
        ExtensionManagerFactory.setManager(myFakeManager);

        LogAnalyzer_3_6 log = new LogAnalyzer_3_6();
        boolean result = log.isValidLogFileName("abcd.txt");

        Assert.assertTrue(result);
    }

    private class FakeExtensionManager implements IExtensionManager {

        public boolean willBeValid = false;
        public Exception willThrow = null;

        @Override
        public boolean isValid(String fileName) {
            return willBeValid;
        }

    }
}
