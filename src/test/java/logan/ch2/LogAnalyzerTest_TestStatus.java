package logan.ch2;
import java.util.Arrays;
import java.util.Collection;



@RunWith(Parameterized.class)
public class LogAnalyzerTest_TestStatus {
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(
				new Object[][] {
					{"badfile.foo", false},
					{"goodfile.slf", true}
				}
		);
	}
	
	@Parameter(0)
	public String fileName;

	@Parameter(1)
	public boolean testExcepted;
	
	@Test
	public void isValidFileName_whenCalled_changesWasLastFileNameValid() {
		LogAnalyzer la = buildLogAnalyzer();
		
		la.isValidLogFileName(fileName);
		
		Assert.assertEquals(testExcepted, la.wasLastFileNameValid);
	}
	
	private static LogAnalyzer buildLogAnalyzer() {
		return new LogAnalyzer();
	}
}
