package logan.ch2;
import java.util.Arrays;
import java.util.Collection;

import logan.ch2.LogAnalyzer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class LogAnalyzerTest_Parametered {

	/**
	 * Junit4, Parameterized test.
	 * {@link} https://github.com/junit-team/junit4/wiki/Parameterized-tests {@link}
	 */
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(
				new Object[][] {
					{"fileWithBadExtension.foo", false},
					{"fileWithGoodExtension.slf", true},
					{"fileWithGoodExtension.SLF", true}
				}
		);
	}
	
	/* Field injection. NOT private parameter. */
	@Parameter(0)
	public String fileName;

	@Parameter(1)
	public boolean testExcepted;
	
	/* Constructor injection */
//	private String fileName;
//	private boolean testExcepted;
//	public LogAnalyzerTest_Parametered(String fileName, boolean testExcepted) {
//		this.fileName = fileName;
//		this.testExcepted = testExcepted;
//	}
	
	@Test
	public void isValidLogFileNAme_VariousExtensions_CheckThen() {
		LogAnalyzer analyzer = new LogAnalyzer();
		boolean result = 
				analyzer
				.isValidLogFileName(fileName);
		Assert.assertEquals(testExcepted, result);
	}
}
