package logan.ch2;
import logan.ch2.LogAnalyzer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;


public class LogAnalyzerTest {

	@Test
	public void isValidFileName_BadExtension_ReturnsFalse() {
		/**
		 * Arrange object.
		 */
		LogAnalyzer analyzer = new LogAnalyzer();

		/**
		 * Act object.
		 */
		boolean result = analyzer.isValidLogFileName("fileWithBadExtension.foo");

		/**
		 * Assert expectation.
		 */
		Assert.assertFalse(result);
	}

	@Test
	public void isValidLogFileName_GoodExtensionLowercase_ReturnTrue() {
		LogAnalyzer analyzer = new LogAnalyzer();
		boolean result = 
				analyzer
				.isValidLogFileName("fileWithGoodExtension.slf");
		Assert.assertTrue(result);
	}

	@Test
	public void isValidLogFileName_GoodExtensionUppercase_ReturnsTrue() {
		LogAnalyzer analyzer = new LogAnalyzer();
		boolean result = 
				analyzer
				.isValidLogFileName("fileWithGoodExtension.SLF");
		Assert.assertTrue(result);
	}
}
