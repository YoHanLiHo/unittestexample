package logan.ch2;

import java.util.ArrayList;

import java.log.LogAnalyzer;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class LogAnalyzerTest_ExceptedException {

	public LogAnalyzerTest_ExceptedException() {
		super();
	}

	/**
	 * Junit4, Exception testing
	 * {@link} https://github.com/junit-team/junit4/wiki/Exception-testing {@link}
	 */
	
	/* Try/Catch Idiom */
	@Test
	public void testExceptionMessage() {
		try {
	        new ArrayList<Object>().get(0);
	        Assert.fail("Expected an IndexOutOfBoundsException to be thrown");
	    } catch (IndexOutOfBoundsException anIndexOutOfBoundsException) {
	    	Assert.assertThat(anIndexOutOfBoundsException.getMessage(),  CoreMatchers.is("Index: 0, Size: 0"));
	    }
	}

	/* ExpectedException Rule */
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void isValidFileNAme_emptyFileNAme_throws() {
		LogAnalyzer analyzer = buildLogAnalyzer();
		
		thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(CoreMatchers.startsWith("filename has to be provided"));

        analyzer.isValidLogFileName("");
	}
	
	private static LogAnalyzer buildLogAnalyzer() {
		return new LogAnalyzer();
	}
	
}
