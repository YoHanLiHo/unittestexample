package logan.ch2;
import logan.ch2.MemCalculator;
import org.junit.Assert;
import org.junit.Test;

public class MemCalculatorTest {

	@Test
	public void sum_byDefault_ReturnsZero() {
		MemCalculator calc = new MemCalculator();
		int lastSum = calc.sum();
		Assert.assertEquals(0, lastSum);
	}

	@Test
	public void add_whenCalled_changesSum() {
		MemCalculator calc = new MemCalculator();
		calc.add(1);
		int sum = calc.sum();
		Assert.assertEquals(1, sum);
	}
}