import assertjtest.AssertionsTest;
import logan.ch2.*;
import logan.ch3.LogAnalyzer_3_6Test;
import logan.ch4.FakeWebService;
import logan.ch4.LogAnalyzer2Test;
import logan.ch5.EventListenerTests;
import logan.ch5.LogAnalyzerTests_with_stub_mock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({

        LogAnalyzerTest.class,
        LogAnalyzerTest_ExceptedException.class,
        LogAnalyzerTest_Parametered.class,
        LogAnalyzerTest_TestStatus. class,
        MemCalculatorTest.class,

        LogAnalyzer_3_6Test.class,
        logan.ch3.LogAnalyzerTest.class,

        LogAnalyzer2Test.class,
        logan.ch4.LogAnalyzeTest.class,

        EventListenerTests.class,
        logan.ch5.LogAnalyzerTests.class,
        LogAnalyzerTests_with_stub_mock.class,

        AssertionsTest.class
})

public class AllTestSuite {

}
