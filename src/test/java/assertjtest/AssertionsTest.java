package assertjtest;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class AssertionsTest {

    @Test
    public void assertThat_noOverrideEquals_twoObjectSameFields_isEqual() {
        Student actualStudent = new Student();
        actualStudent.name = "John";
        actualStudent.age = 15;
        Address actualAddress = new Address();
        actualAddress.line = "中央路";
        actualStudent.homeAddress = actualAddress;

        Student expectedStudent = new Student();
        expectedStudent.name = "John";
        expectedStudent.age = 15;
        Address expectedAddress = new Address();
        expectedAddress.line = "中央路";
        expectedStudent.homeAddress = expectedAddress;

        Assertions.assertThat(actualStudent).isEqualToComparingFieldByFieldRecursively(expectedStudent);
    }

    @Test
    public void assertThat_noOverideEquels_twoObjectDiffFields_isNotEqual() {
        Student actualStudent = new Student();
        actualStudent.name = "John";
        actualStudent.age = 15;
        Address actualAddress = new Address();
        actualAddress.line = "中央路";
        actualStudent.homeAddress = actualAddress;

        Student expectedStudent = new Student();
        expectedStudent.name = "John";
        expectedStudent.age = 15;

        Assertions.assertThat(actualStudent).isNotEqualTo(expectedStudent);
    }

    @Test
    public void aseertThat_noOverideEquels_twoObjectDiffFields_ignoreGivenFields_isEqual() {
        Student actualStudent = new Student();
        actualStudent.name = "John";
        actualStudent.age = 15;
        Address actualAddress = new Address();
        actualAddress.line = "中央路";
        actualStudent.homeAddress = actualAddress;

        Student expectedStudent = new Student();
        expectedStudent.name = "John";
        expectedStudent.age = 15;

        Assertions.assertThat(actualStudent)
                  .isEqualToIgnoringGivenFields(expectedStudent, "homeAddress");
    }

    @Test
    public void assertThat_overrideEquals_twoObjectDiffFields_isNotEqual() {
        Teacher actualTeacher = new Teacher();
        actualTeacher.age = 30;
        actualTeacher.name = "Bill";
        Department actualDept = new Department();
        actualDept.title = "Math";
        actualTeacher.dept = actualDept;

        Teacher expectedTeacher = new Teacher();
        expectedTeacher.age = 45;
        expectedTeacher.name = "Smith";
        Department expectedDept = new Department();
        expectedDept.title = "Math";
        expectedTeacher.dept = expectedDept;

        Assertions.assertThat(actualTeacher).isNotEqualTo(expectedTeacher);
    }

    @Test
    public void assertThat_overrideEquals_twoObjectSameFields_isEqual() {
        Teacher actualTeacher = new Teacher();
        actualTeacher.age = 30;
        actualTeacher.name = "Bill";
        Department actualDept = new Department();
        actualDept.title = "Math";
        actualTeacher.dept = actualDept;

        Teacher expectedTeacher = new Teacher();
        expectedTeacher.age = 30;
        expectedTeacher.name = "Bill";
        Department expectedDept = new Department();
        expectedDept.title = "Math";
        expectedTeacher.dept = expectedDept;

        Assertions.assertThat(actualTeacher).isEqualToComparingFieldByFieldRecursively(expectedTeacher);
    }

    @Test
    public void reflectionEquals_addExcludeFields_overideEquels_twoObjectDiffFields_true() {
        Teacher actualTeacher = new Teacher();
        actualTeacher.age = 30;
        actualTeacher.name = "Bill";
        Department actualDept = new Department();
        actualDept.title = "Math";
        actualTeacher.dept = actualDept;

        Teacher expectedTeacher = new Teacher();
        expectedTeacher.age = 30;
        expectedTeacher.name = "Bill";
        Department expectedDept = new Department();
        expectedDept.title = "Chinese";
        expectedTeacher.dept = expectedDept;

        Assertions.assertThat(actualTeacher)
                  .isEqualToIgnoringGivenFields(expectedTeacher, "dept");
    }
}