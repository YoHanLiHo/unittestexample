package logan.ch5;

public class LogAnalyzer {

    public int minNameLength;
    private ILogger logger;

    public LogAnalyzer(ILogger logger) {
        this.logger = logger;
    }

    public boolean analyze(String fileName) {
        if (fileName.length() < minNameLength) {
            return false;
        }
        return true;
    }
}
