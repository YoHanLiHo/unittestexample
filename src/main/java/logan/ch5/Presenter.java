package logan.ch5;


/**
 * this class is event listener.
 */
public class Presenter {

    public Presenter() {
        super();
    }

    public void onLoad(IView view) {
        view.render("hello");
    }

}
