package logan.ch5;

public class LogAnalyzer2
{
    private ILogger _logger;
    private IWebService _webService;
    public int minNameLength;

    public LogAnalyzer2(ILogger logger, IWebService webService)
    {
        this._logger = logger;
        this._webService = webService;
    }

    public void analyze(String fileName)
    {
        if (fileName.length() < minNameLength)
        {
            try {
                _logger.logError("file name is too short.");
            }
            catch (RuntimeException e)
            {
                System.out.println("occurs RuntimeException: " + e.getMessage());
                _webService.write("error from Logger:" + e);
            }
        }
    }

    public int getMinNameLength() {
        return minNameLength;
    }

    public void setMinNameLength(int minNameLength) {
        this.minNameLength = minNameLength;
    }
}
