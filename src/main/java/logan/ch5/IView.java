package logan.ch5;

public interface IView {

    String render(String text);
}
