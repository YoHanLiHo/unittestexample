package logan.ch5;

public interface IFileNameRules {

    boolean isValidLogFileName(String fileName);
}
