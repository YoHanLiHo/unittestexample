package logan.ch2;

public class LogAnalyzer {

	public boolean wasLastFileNameValid;
	
	public boolean isValidLogFileName(String fileName) {

		wasLastFileNameValid = false;
		if (fileName == null | fileName.isEmpty()) {
			throw new IllegalArgumentException("filename has to be provided");
		}
		
		if (!(fileName.endsWith(".SLF") | fileName.endsWith(".slf"))) {
			return false;
		}
		
		wasLastFileNameValid = true;
		return true;
	}
}

