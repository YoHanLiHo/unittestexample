package logan.ch4;

public interface IWebService {

    void logError(String message) throws Exception;
}
