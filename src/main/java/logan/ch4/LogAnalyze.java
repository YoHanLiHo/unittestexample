package logan.ch4;

public class LogAnalyze {

    private IWebService service;

    public LogAnalyze(IWebService service) {
        this.service = service;
    }

    public void analyze(String fileName) throws Exception {
        if (fileName.length() < 8) {
            service.logError("Filename too short:" + fileName);
        }
    }
}
