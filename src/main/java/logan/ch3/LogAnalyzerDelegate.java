package logan.ch3;

public class LogAnalyzerDelegate {

    public boolean isValidLogFileName(String fileName) {

        IExtensionManager manager = new FileExtensionManager();

        return manager.isValid(fileName);
    }
}
