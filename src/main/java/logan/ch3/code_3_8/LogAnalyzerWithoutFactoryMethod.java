package logan.ch3.code_3_8;

public class LogAnalyzerWithoutFactoryMethod {

    public boolean isValidLogFileName(String fileName) {
        return this.isValid(fileName);
    }

    protected boolean isValid(String fileName) {
        FileExtensionManager mgr = new FileExtensionManager();
        return mgr.isValid(fileName);
    }
}