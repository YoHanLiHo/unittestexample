package logan.ch3;

/**
 * 此 calss 針對程式碼 3.6 改寫，用來解說 factory method 對 unit test 的作用
 */
public class LogAnalyzer_3_6 {

    private IExtensionManager manager;

    public LogAnalyzer_3_6() {
        /**
         * constructor 建構物件的時候透過 factory method 取得 manager.
         */
        manager = ExtensionManagerFactory.getInstance();
    }

    public boolean isValidLogFileName(String fileName) {
        boolean result = false;
        try {
            result = manager.isValid(fileName) && fileName.length() > 7;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
