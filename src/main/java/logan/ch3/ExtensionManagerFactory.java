package logan.ch3;

public class ExtensionManagerFactory {

    private static IExtensionManager customManager = null;

    /**
     * 生成物件的時候如果 customerManager != null 就用預設 manager;
     * 如果 customerManager == null 就用來生成物件
     * @return
     */
    public static IExtensionManager getInstance() {
        if (customManager != null) {
            return customManager;
        } else {
            /**
             * 預設 manager
             */
            return new FileExtensionManager();
        }
    }

    public static void setManager(IExtensionManager mgr) {
        customManager = mgr;
    }
}
