package logan.ch3;

public class LogAnalyzerUsingFactoryMethod {

    public boolean isValidLogFileName(String fileName) {
        return getManager().isValid(fileName);
    }

    protected IExtensionManager getManager() {
        return new FileExtensionManager();
    }
}
