package logan.ch3;

public interface IExtensionManager {

    boolean isValid(String fileName);
}
