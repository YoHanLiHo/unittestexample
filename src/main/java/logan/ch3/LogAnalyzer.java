package logan.ch3;

public class LogAnalyzer {

	/**
	 * field injection, setter injection.
	 */
	private IExtensionManager manager;

	public LogAnalyzer() {
		manager = new FileExtensionManager();
	}

	public IExtensionManager getManager() {
		return manager;
	}
	public void setManager(IExtensionManager manager) {
		this.manager = manager;
	}


	/**
	 * constructor injection.
	 */
	public LogAnalyzer(IExtensionManager manager) {
		this.manager = manager;
	}

	public boolean isValidLogFileName(String fileName) {
		boolean result = false;
		try {
			result = manager.isValid(fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}