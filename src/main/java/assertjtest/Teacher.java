package assertjtest;

import java.util.Objects;

public class Teacher {

    public String name;
    public int age;
    public Department dept;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return age == teacher.age &&
                name.equals(teacher.name) &&
                dept.equals(teacher.dept);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, dept);
    }
}
